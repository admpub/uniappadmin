module base_service

go 1.14

require (
	github.com/GUAIK-ORG/go-snowflake v0.0.0-20200116064823-220c4260e85f
	github.com/astaxie/beego v1.12.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/text v0.3.2 // indirect
)
